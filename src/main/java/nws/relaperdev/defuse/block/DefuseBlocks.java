package nws.relaperdev.defuse.block;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;
import net.minecraft.block.Block;
import net.minecraft.block.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public abstract class DefuseBlocks {
    private DefuseBlocks() { }

    public static final Block DEFUSE_ORE = new Block(FabricBlockSettings.of(Material.STONE).strength(4.0f));
    public static final Block DEFUSE_MACHINE_FRAME = new Block(FabricBlockSettings.of(Material.METAL).strength(8.5f).requiresTool().breakByTool(FabricToolTags.PICKAXES, 4).nonOpaque());
    public static final DefuseCrystalBlock DEFUSE_CRYSTAL_BLOCK = new DefuseCrystalBlock(FabricBlockSettings.of(Material.GLASS).strength(4.5f).requiresTool().breakByTool(FabricToolTags.PICKAXES, 2));
    public static final DefuseGrinderBlock DEFUSE_GRINDER = new DefuseGrinderBlock(FabricBlockSettings.of(Material.METAL).strength(2.5f));

    public static void registerBlocks() {
        Registry.register(Registry.BLOCK, new Identifier("defuse", "defuse_ore"), DEFUSE_ORE);
        Registry.register(Registry.BLOCK, new Identifier("defuse", "defuse_machine_frame"), DEFUSE_MACHINE_FRAME);
        Registry.register(Registry.BLOCK, new Identifier("defuse", "defuse_crystal_block"), DEFUSE_CRYSTAL_BLOCK);
        Registry.register(Registry.BLOCK, new Identifier("defuse", "defuse_grinder"), DEFUSE_GRINDER);
    }

    public static void registerBlockItems() {
        Registry.register(Registry.ITEM, new Identifier("defuse", "defuse_ore"), new BlockItem(DEFUSE_ORE, new FabricItemSettings().group(ItemGroup.BUILDING_BLOCKS)));
        Registry.register(Registry.ITEM, new Identifier("defuse", "defuse_machine_frame"), new BlockItem(DEFUSE_MACHINE_FRAME, new FabricItemSettings().group(ItemGroup.DECORATIONS)));
        Registry.register(Registry.ITEM, new Identifier("defuse", "defuse_crystal_block"), new BlockItem(DEFUSE_CRYSTAL_BLOCK, new FabricItemSettings().group(ItemGroup.BUILDING_BLOCKS)));
        Registry.register(Registry.ITEM, new Identifier("defuse", "defuse_grinder"), new BlockItem(DEFUSE_GRINDER, new FabricItemSettings().group(ItemGroup.DECORATIONS)));
    }
}
