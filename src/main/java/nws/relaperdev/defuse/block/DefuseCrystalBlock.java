package nws.relaperdev.defuse.block;

import net.minecraft.block.Block;
import net.minecraft.block.Material;

public class DefuseCrystalBlock extends Block {

    public DefuseCrystalBlock(Settings settings) {
        super(Settings.of(Material.GLASS).nonOpaque());
    }
}
