package nws.relaperdev.defuse.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.explosion.Explosion;
import nws.relaperdev.defuse.item.DefuseItems;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DefuseGrinderBlock extends Block {
    public DefuseGrinderBlock(Settings settings) {
        super(settings);
        setDefaultState(getStateManager().getDefaultState().with(CHARGED, false));
    }

    private static final Logger logger = LogManager.getLogger();
    public static final BooleanProperty CHARGED = BooleanProperty.of("charged");

    @Override
    protected void appendProperties(StateManager.Builder<Block, BlockState> builder) {
        builder.add(CHARGED);
    }

    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
        if (player.getInventory().getMainHandStack().getItem() == Items.TNT) {
            player.getInventory().getMainHandStack().decrement(1);
            if (!state.get(CHARGED)) {
                if (world.isClient) {
                    logger.info("charging me");
                    world.playSound(null, pos, SoundEvents.BLOCK_GRASS_PLACE, SoundCategory.BLOCKS, 10f, 1f);
                }
                world.setBlockState(pos, state.with(CHARGED, true));
                return ActionResult.CONSUME;
            } else {
                if (world.isClient) player.sendMessage(new TranslatableText("block.defuse_grinder.already_charged"), true);
                return ActionResult.SUCCESS;
            }
        } else if (player.getInventory().getMainHandStack().getItem() == DefuseItems.DEFUSE_CRYSTAL) {
            if (state.get(CHARGED)) {
                player.getInventory().getMainHandStack().decrement(1);
                world.createExplosion(null, pos.getX(), pos.getY(), pos.getZ(), 4.5f, true, Explosion.DestructionType.BREAK);
                player.getInventory().offerOrDrop(new ItemStack(DefuseItems.SHAPELESS_DEFUSE_CRYSTAL));
                return ActionResult.CONSUME;
            } else {
                if (world.isClient) player.sendMessage(new TranslatableText("block.defuse_grinder.not_charged"), true);
                return ActionResult.SUCCESS;
            }
        }
        return ActionResult.SUCCESS;
    }
}
