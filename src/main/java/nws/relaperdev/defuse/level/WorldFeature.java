package nws.relaperdev.defuse.level;

import net.fabricmc.fabric.api.biome.v1.BiomeModifications;
import net.fabricmc.fabric.api.biome.v1.BiomeSelectors;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.BuiltinRegistries;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.gen.GenerationStep;
import net.minecraft.world.gen.YOffset;
import net.minecraft.world.gen.decorator.Decorator;
import net.minecraft.world.gen.decorator.RangeDecoratorConfig;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.heightprovider.UniformHeightProvider;
import nws.relaperdev.defuse.block.DefuseBlocks;

public abstract class WorldFeature {
    private WorldFeature() {}

    private static ConfiguredFeature<?, ?> ORE_DEFUSE_OVERWORLD = Feature.ORE
    .configure(new OreFeatureConfig(
      OreFeatureConfig.Rules.BASE_STONE_OVERWORLD,
      DefuseBlocks.DEFUSE_ORE.getDefaultState(),
      3)) // vein size
    .decorate(Decorator.RANGE.configure(new RangeDecoratorConfig(
    UniformHeightProvider.create(
    YOffset.fixed(5), 
    YOffset.fixed(15)))))
    .spreadHorizontally()
    .repeat(1); // number of veins per chunk

    public static void registerFeatures() {
        RegistryKey<ConfiguredFeature<?, ?>> oreWoolOverworld = RegistryKey.of(Registry.CONFIGURED_FEATURE_KEY,
        new Identifier("defuse", "ore_defuse_overworld"));
    Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, oreWoolOverworld.getValue(), ORE_DEFUSE_OVERWORLD);
    BiomeModifications.addFeature(BiomeSelectors.foundInOverworld(), GenerationStep.Feature.UNDERGROUND_ORES, oreWoolOverworld);
    }
}
