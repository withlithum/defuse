package nws.relaperdev.defuse;

import net.fabricmc.api.ModInitializer;
import nws.relaperdev.defuse.block.DefuseBlocks;
import nws.relaperdev.defuse.item.DefuseItems;
import nws.relaperdev.defuse.level.WorldFeature;

// static
public class Defuse implements ModInitializer {

    @Override
    public void onInitialize() {
        DefuseBlocks.registerBlocks();
        DefuseBlocks.registerBlockItems();
        DefuseItems.registerItems();
        WorldFeature.registerFeatures();
    }
    
}
