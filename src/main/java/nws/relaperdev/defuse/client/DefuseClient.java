package nws.relaperdev.defuse.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.minecraft.client.render.RenderLayer;
import nws.relaperdev.defuse.block.DefuseBlocks;

public class DefuseClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        BlockRenderLayerMap.INSTANCE.putBlock(DefuseBlocks.DEFUSE_CRYSTAL_BLOCK, RenderLayer.getTranslucent());
        BlockRenderLayerMap.INSTANCE.putBlock(DefuseBlocks.DEFUSE_MACHINE_FRAME, RenderLayer.getTranslucent());
    }
}
