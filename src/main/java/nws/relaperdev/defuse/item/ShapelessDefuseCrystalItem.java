package nws.relaperdev.defuse.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.particle.ParticleEffect;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;

public class ShapelessDefuseCrystalItem extends Item {
    public ShapelessDefuseCrystalItem(Settings settings) {
        super(settings);
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext context) {
        if (context.getWorld().isClient) {
            context.getWorld().playSound(null, context.getBlockPos(), SoundEvents.BLOCK_GLASS_BREAK, SoundCategory.PLAYERS, 10, 1);
            context.getPlayer().sendMessage(new TranslatableText("item.defuse.shapeless_defuse_crystal.break"), false);
        }

        context.getStack().decrement(1);
        return ActionResult.CONSUME;
    }
}
