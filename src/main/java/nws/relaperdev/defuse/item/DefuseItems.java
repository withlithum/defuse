package nws.relaperdev.defuse.item;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

// static
public abstract class DefuseItems {
    private DefuseItems() { }

    public static final Item DEFUSE_CRYSTAL = new Item(new FabricItemSettings().group(ItemGroup.MISC));
    public static final ShapelessDefuseCrystalItem SHAPELESS_DEFUSE_CRYSTAL = new ShapelessDefuseCrystalItem(new FabricItemSettings().group(ItemGroup.MISC));

    public static void registerItems() {
        Registry.register(Registry.ITEM, new Identifier("defuse", "defuse_crystal"), DEFUSE_CRYSTAL);
        Registry.register(Registry.ITEM, new Identifier("defuse", "shapeless_defuse_crystal"), SHAPELESS_DEFUSE_CRYSTAL);
    }
}